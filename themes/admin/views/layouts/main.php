<!DOCTYPE html>
<html>
<head>
    <script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
    <title>AdminLTE | Dashboard</title>
    <meta charset="UTF-8" content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- jvectormap -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet"
          type="text/css"/>
    <!-- Date Picker -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/datepicker/datepicker3.css" rel="stylesheet"
          type="text/css"/>
    <!-- Daterange picker -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/admin/" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <?php echo Yii::app()->name;?>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->

                <li class="user user-menu">
                    <a href="<?php echo Yii::app()->getBaseUrl(true); ?>">
                        <i class="fa fa-arrow-left"></i>
                        <span>Back to site</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <?php
                    $userID = Yii::app()->user->id;
                    $userName = Yii::app()->user->name;
                    $fileName = "{$userID}-{$userName}";  // random number + file name
                    $fileName = md5($fileName).'.jpg';
                    $imagePath = 'users_img/small/'.$fileName;

                    if (file_exists($imagePath)) {
                    echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/small/'.$fileName, "User foto");}
                     else {
                    echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/default_user.png', "Default foto");}
                    ?>
                </div>

                <div class="pull-left info">
                    <p>Hello, <?php echo Yii::app()->user->name ?></p>

                    <p><a href="#"><i class="fa fa-circle text-success"></i> Online</a></p>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="active ">
                    <a href="<?php echo Yii::app()->baseUrl; ?>/admin/dashboard/index">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Ideas</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/admin/ideas/index"><i class="fa fa-file"></i>All
                                Ideas</a></li>
                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/admin/ideas/create"><i class="fa fa-file"></i>Create
                                Idea</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/admin/users/index"><i class="fa fa-file"></i>All
                                Users</a></li>
                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/admin/users/create"><i class="fa fa-file"></i>Create
                                User</a></li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <ol class="breadcrumb">
                <?php if(isset($this->breadcrumbs)):?>
                            <?php $this->widget('ext.MyBreadcrumb.MyCBreadcrumbs', array(
                                'links'=>$this->breadcrumbs,
                            )); ?><!-- breadcrumbs -->
                <?php endif?>
                <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li> -->
            </ol>
            <h1 style="padding: 11px;">
                <?php echo $this->nameCont;?>
                <small>Control panel</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php echo $content; ?>

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->


<!-- jQuery 2.0.2 -->

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/highcharts.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/artem_script.js"></script>

<!-- jQuery UI 1.10.3 -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/AdminLTE/app.js" type="text/javascript"></script>

</body>
</html>