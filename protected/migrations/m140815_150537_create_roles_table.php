<?php

class m140815_150537_create_roles_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_roles', array(
            'id' => 'pk',
            'name' => 'varchar(25) NOT NULL',
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_roles');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}