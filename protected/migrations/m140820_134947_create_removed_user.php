<?php

class m140820_134947_create_removed_user extends CDbMigration
{
	public function up()
	{
		$this->insert( 'tbl_users', array(
			'id' => '777',
			'role' => '1',
			'login' => 'Removed User',
			'password' => 'pwd',
			'status_confirmed' => '1'
		));
	}

	public function down()
	{
		$this->delete( 'tbl_users', '777' );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}