<?php

class m140815_151035_insert_roles_table extends CDbMigration
{
	public function up()
	{
        $this->insert('tbl_roles', array(
            'id' => '1',
            'name' => 'user',
        ));
        $this->insert('tbl_roles', array(
            'id' => '2',
            'name' => 'moderator',
        ));
        $this->insert('tbl_roles', array(
            'id' => '3',
            'name' => 'admin',
        ));
	}

	public function down()
	{
        $this->delete('tbl_roles', '1');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}