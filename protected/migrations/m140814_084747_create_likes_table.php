<?php

class m140814_084747_create_likes_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_likes', array(
            'id' => 'pk',
            'user_id' => 'integer unsigned NOT NULL',
            'idea_id' => 'integer unsigned NOT NULL',
        ), 'ENGINE=InnoDB');
	}

	public function down()
	{
        $this->dropTable('tbl_likes');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}