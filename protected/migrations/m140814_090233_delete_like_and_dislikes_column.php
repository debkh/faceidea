<?php

class m140814_090233_delete_like_and_dislikes_column extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('tbl_ideas', 'like_count');
        $this->dropColumn('tbl_ideas', 'dislike_count');
	}

	public function down()
	{
        $this->addColumn('tbl_ideas', 'like_count', 'integer unsigned NOT NULL');
        $this->addColumn('tbl_ideas', 'dislike_count', 'integer unsigned NOT NULL');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}