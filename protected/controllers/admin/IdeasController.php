<?php

class IdeasController extends AdminController
{
    public $nameCont = "Ideas";

    public $layout = '//layouts/main';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'index', 'delete', 'view', 'downloadfile'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $model_attachments = new Attachments();
        $attachments = $model_attachments->findAllByAttributes(array('idea_id' => $id));

        $this->render('view', array(
            'attachments' => $attachments,
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Ideas;

        if (isset($_POST['Ideas'])) {
            $attachments = CUploadedFile::getInstancesByName('attachments');
            $model->attributes = $_POST['Ideas'];
            $model->date = new CDbExpression('NOW()');
            $model->user_id = Yii::app()->user->getId();

            if ($model->save()) {
                if ($attachments) {

                    foreach ($attachments as $key => $attachment) {
                        $ext = pathinfo($attachment);
                        $newAttachment = new Attachments();
                        $newAttachment->idea_id = $model->id;
                        $newAttachment->name = md5($attachment) . '.' . $ext['extension'];
                        $newAttachment->original_name = $attachment;

                        if ($newAttachment->save()) {
                            $attachment->saveAs(Yii::getPathOfAlias('webroot') . '/attachments/' . md5($attachment) . '.' . $ext['extension']);
                        }
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));

            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Ideas'])) {
            $attachments = CUploadedFile::getInstancesByName('attachments');
            $model->attributes = $_POST['Ideas'];

            if ($model->save()) {
                if ($attachments) {
                    foreach ($attachments as $key => $attachment) {
                        $ext = pathinfo($attachment);
                        $newAttachment = new Attachments();
                        $newAttachment->idea_id = $model->id;
                        $newAttachment->name = md5($attachment) . '.' . $ext['extension'];
                        $newAttachment->original_name = $attachment;

                        if ($newAttachment->save()) {
                            $attachment->saveAs(Yii::getPathOfAlias('webroot') . '/attachments/' . md5($attachment) . '.' . $ext['extension']);
                        }
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));

            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
//    public function actionIndex()
//    {
//        $model = new Ideas();
//        $data = $model->findAll();
//
//        $this->render('index', array(
//            'dataProvider' => $data,
//        ));
//    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Ideas('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Ideas']))
            $model->attributes = $_GET['Ideas'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Ideas the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Ideas::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Ideas $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ideas-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiondownloadfile()
    {
        $src = $_GET['attachment'];
        $original_name = $_GET['original_name'];
        $upload_path = Yii::getPathOfAlias('webroot') . '/attachments/';

        if (file_exists($upload_path . $src)) {
            Yii::app()->getRequest()->sendFile($original_name, file_get_contents($upload_path . $src));
        } else {
//            $this->render('download 404');
            return;
        }
    }
}
