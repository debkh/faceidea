<?php

class UsersController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

    public $nameCont = "Users";

	public $layout = '//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('update', 'index', 'delete', 'view','create','confirmation', 'unconfirm'),
				'users' => array('admin'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $origdata = Users::model()->findByPk($id);

		if (isset($_POST['Users'])) {
            $userID = $model->id;
            $userName = $model->name;
			$model->attributes = $_POST['Users'];
			$uploadedFile=CUploadedFile::getInstance($model,'image');
			$fileName = "{$userID}-{$userName}";  // random number + file name
			$fileName = md5($fileName);
            $model->image = $fileName.'.jpg';
            if ($model->password !== $origdata->password ){$model->password = sha1( $model->password );}
            if ($model->save()) {
				if(!empty($uploadedFile))  // check if uploaded file is set or not
				{
                    $model->image = $fileName.'.jpg';
                        $uploadedFile->saveAs(Yii::app()->basePath.'/../users_img/'.$fileName.'.jpg');
                    $image = Yii::app()->image->load(Yii::app()->basePath.'/../users_img/'.$fileName.'.jpg');
                    $image->quality(75);
                    $image->resize(300,300);
                    $image->crop(200, 200);
                    $image->save(Yii::app()->basePath.'/../users_img/small/'.$fileName.'.jpg'); // or $image->save('images/small.jpg');
				}
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

    public function actionCreate(){
        $model=new Users;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->role = 1;

        if(isset($_POST['Users']))
        {
            $model->attributes=$_POST['Users'];
            $model->password = sha1( $model->password );
            $rnd = rand(0,9999);
            $uploadedFile=CUploadedFile::getInstance($model,'image');
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name

            if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->image = $fileName;
                $uploadedFile->saveAs(Yii::app()->basePath.'/../users_img/'.$fileName);
                $image = Yii::app()->image->load(Yii::app()->basePath.'/../users_img/'.$fileName);
                $image->quality(75);
                $image->resize(300,300);
                $image->crop(200, 200);
                $image->save(Yii::app()->basePath.'/../users_img/small/'.$fileName); // or $image->save('images/small.jpg');
            }

            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

	public function actionDelete()
	{
		if( isset( $_POST['id'] ) ) {
			$ideas = Ideas::model()->findAllByAttributes( array( 'user_id' => $_POST['id'] ) );
			foreach ( $ideas as $idea ) {
				$idea->user_id = '777';
				$idea->save();
			}
			$comments = Comments::model()->findAllByAttributes( array( 'user_id' => $_POST['id'] ) );
			foreach ( $comments as $comment ) {
				$comment->user_id = '777';
				$comment->save();
			}
			$this->loadModel($_POST['id'])->delete();
		}
		die();
	}

	public function actionConfirmation() {
		if( isset( $_POST['id'] ) ) {
			$user = $this->loadModel($_POST['id']);
			$user->status_confirmed = 1;
			if( $user->save() )
				echo 'Success';
			else
				echo 'Fail';
			die();
		}
	}

    public function actionUnConfirm() {
        if( isset( $_POST['id'] ) ) {
            $this->loadModel($_POST['id'])->delete();
                echo 'Success';
            die();
        }
    }

    /**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new Users('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Users']))
			$model->attributes = $_GET['Users'];

		$this->render('admin', array(
			'users' => $model,
		));
	}

	public function loadModel($id)
	{
		$model = Users::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
