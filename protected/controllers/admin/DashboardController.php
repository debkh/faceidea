<?php

class DashboardController extends AdminController
{
    public $nameCont = "Dashboard";

	public function actionIndex()
	{
		$criteriaByMonths = new CDbCriteria();
		$criteriaByMonths->select = 'MONTH(date)-1 as m, COUNT(*) as c';
		$criteriaByMonths->condition = 'YEAR(date) = YEAR(NOW())';
		$criteriaByMonths->group = 'MONTH(date)';
		$ideasByMonths = Ideas::model()->findAll($criteriaByMonths);

		$criteriaByStatus = new CDbCriteria();
		$criteriaByStatus->select = 'status_confirmed, COUNT(*) as c';
		$criteriaByStatus->group = 'status_confirmed';

		$users = Users::model()->findAllByAttributes( array( 'status_confirmed' => '0' ) );

		$ideasByStatus = Ideas::model()->findAll($criteriaByStatus);
		$this->render('index', array(
			'ideasByMonths' => $ideasByMonths,
			'ideasByStatus' => $ideasByStatus,
			'users' => $users
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}