<?php
class SiteController extends Controller
{

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',

            ),
            'yiichat'=>array('class'=>'YiiChatAction'),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $model = new Users;
        $flag = true;
        if ( isset( $_POST['Users'] ) ) {
            if ( isset( $_POST['Register'] ) && !$model->registration( $_POST['Users'] ) )
                $flag = false;
            if ( $flag && $model->login())
                $this->redirect( Yii::app()->homeUrl );
        }
        $this->render( 'enter', array( 'model' => $model ) );
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionLike() {
        if (isset($_POST['id_idea'])) {
            preg_match("/(\d*)_(\d*)/", $_POST['id_idea'], $idArray);
            $idea_id = $idArray[1];
            $user_id = $idArray[2];

            $db_id = Yii::app()->db->createCommand()
                ->select('count(*) count')
                ->from('tbl_likes')
                ->where('user_id=:id1 and idea_id=:id2', array(':id1'=>$user_id, ':id2'=>$idea_id))
                ->queryRow();

            if ($db_id['count'] > 0)
            {
                Yii::app()->db->createCommand()->delete('tbl_likes', 'user_id=:id1 and idea_id=:id2', array(':id1'=>$user_id, ':id2'=>$idea_id));
                $like_status = false;
            }
            else
            {
                $model              = new Likes;
                $model->idea_id     = $idea_id;
                $model->user_id     = $user_id;
                $model->save();
                $like_status = true;
            }

            $db_id_count = Yii::app()->db->createCommand()
                ->select('count(*) count')
                ->from('tbl_likes')
                ->where('idea_id=:id1', array(':id1'=>$idea_id))
                ->queryRow();

            echo json_encode(array(
                'success'=>true,
                'message'=>$db_id_count['count'],
                'imlike'=>$like_status,
            ));
            Yii::app()->end();
        }
        else {
            echo json_encode(array(
                'success'=>false,
                'message'=>'Db error'
            ));
            Yii::app()->end();
        }
    }
}