<?php

class IdeasController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'nulls', 'week', 'view', 'downloadfile', 'create', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        if ( isset( $_POST['Reply'] ) ) {
            $newComment = new Comments();
            $newComment->date = new CDbExpression('NOW()');
            $newComment->idea_id = $model->id;
            $newComment->user_id = Yii::app()->user->getId();
            $newComment->content = $_POST['Comments']['content'];
            $newComment->parent_comment_id = $_POST['comment_parent'];
            if ( ! $newComment->save() ) echo 'ERROR!';

        }
        $comments = $model->getComments();
        $model_attachments = new Attachments();
        $attachments = $model_attachments->findAllByAttributes(array('idea_id' => $id));

        $this->render('view', array(
            'attachments' => $attachments,
            'model' => $model,
            'comments' => $comments,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Ideas;

        if (isset($_POST['Ideas'])) {
            $attachments = CUploadedFile::getInstancesByName('attachments');
            $model->attributes = $_POST['Ideas'];
            $model->date = new CDbExpression('NOW()');
            $model->user_id = Yii::app()->user->getId();


            if ($model->save()) {
                if ($attachments) {

                    foreach ($attachments as $key => $attachment) {
                        $ext = pathinfo($attachment);
                        $newAttachment = new Attachments();
                        $newAttachment->idea_id = $model->id;
                        $newAttachment->name = md5($attachment) . '.' . $ext['extension'];
                        $newAttachment->original_name = $attachment;

                        if ($newAttachment->save()) {
                            $attachment->saveAs(Yii::getPathOfAlias('webroot') . '/attachments/' . md5($attachment) . '.' . $ext['extension']);
                        }
                    }
                }

                $email = Yii::app()->email;
                $email->to = 'webfix@ukr.net';//'webmaster@example.com';
                $email->subject = 'New Idea';
                $email->view = 'myview';
                $var1 = Yii::app()->request->getBaseUrl(true).'/ideas/'.$model->id;
                $title = $model->title;
                $name = Yii::app()->user->getName();
                $email->viewVars = array('var1'=>$var1, 'title'=>$title, 'name'=>$name);
                $email->send();

                $this->redirect(array('view', 'id' => $model->id));



            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Ideas'])) {
            $attachments = CUploadedFile::getInstancesByName('attachments');
            $model->attributes = $_POST['Ideas'];

            if ($model->save()) {
                if ($attachments) {
                    foreach ($attachments as $key => $attachment) {
                        $ext = pathinfo($attachment);
                        $newAttachment = new Attachments();
                        $newAttachment->idea_id = $model->id;
                        $newAttachment->name = md5($attachment) . '.' . $ext['extension'];
                        $newAttachment->original_name = $attachment;

                        if ($newAttachment->save()) {
                            $attachment->saveAs(Yii::getPathOfAlias('webroot') . '/attachments/' . md5($attachment) . '.' . $ext['extension']);
                        }
                    }
                }
                $this->redirect(array('view', 'id' => $model->id));

            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if ( Yii::app()->user->isGuest )
            $this->redirect( array('site/login') );

        $criteria=new CDbCriteria(array('order'=> 'id DESC'));
        $count=Ideas::model()->count($criteria);
        $pages=new CPagination($count);
        $pages->pageSize=10;
        $pages->applyLimit($criteria);
        $data=Ideas::model()->findAll($criteria);

        $model = new Likes();
        $like = $model->findAll(array(
            'select'=>'id, idea_id',
            'condition'=>'user_id=:user_id',
            'params'=>array(':user_id'=>Yii::app()->user->id)));
        $likesArray = CHtml::listData($like, 'id', 'idea_id');

        $model = new Users();
        $users= $model->findAll();
        $this->render('index', array(
            'ideas' => $data,
            'userLikes' => $likesArray,
            'author' => $users,
            'pages' => $pages
        ));
    }

    public function actionNulls()
    {
        if ( Yii::app()->user->isGuest )
            $this->redirect( array('site/login') );

        $criteria=new CDbCriteria();
        $criteria->with = (array('relatedLikes'=>array(
            'select' => 'relatedLikes.idea_id AS t1_c2',
            'group' => 't.id',
            'having' => 'COUNT(relatedLikes.idea_id) = 0',
        )));

                $count=Ideas::model()->count($criteria);
                //$pages=new CPagination($count);
                // results per page
                $pages=new CPagination($count);
                $pages->pageSize=10;
                $pages->applyLimit($criteria);

        $model = new Ideas();

        $data = $model->with(array('relatedLikes'=>array(
                'select' => 'relatedLikes.idea_id AS t1_c2',
                'group' => 't.id',
                'having' => 'COUNT(relatedLikes.idea_id) = 0',
            )))->findAll();

        /*----------------------------------------------------------------------------------------------------*
        $criteria=new CDbCriteria();
        $pages=new CPagination($data);
        $pages->pageSize=10;
        $pages->applyLimit($criteria);
        /*----------------------------------------------------------------------------------------------------*/

        $model = new Likes();
        $like = $model->findAll(array(
            'select'=>'id, idea_id',
            'condition'=>'user_id=:user_id',
            'params'=>array(':user_id'=>Yii::app()->user->id)));
        $likesArray = CHtml::listData($like, 'id', 'idea_id');

//        var_dump(array_search('12', $likesArray));
//        foreach ($data as $key => $value) {
//            (array_search('12', $likesArray) != false) ? $data[$key]->userLike = true : $data[$key]->userLike = false;
//        }
//        print_r($data);

        $this->render('index', array(
            'ideas' => $data,
            'userLikes' => $likesArray,
            'pages' => $pages
        ));
    }

    public function actionWeek()
    {
        if ( Yii::app()->user->isGuest )
            $this->redirect( array('site/login') );

        $criteria=new CDbCriteria(array('order'=> 'id DESC','condition'=>'date >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)'));
        $count=Ideas::model()->count($criteria);
        $pages=new CPagination($count);
        $pages->pageSize=10;
        $pages->applyLimit($criteria);
        $data=Ideas::model()->findAll($criteria);

        $model = new Likes();
        $like = $model->findAll(array(
            'select'=>'id, idea_id',
            'condition'=>'user_id=:user_id',
            'params'=>array(':user_id'=>Yii::app()->user->id)));
        $likesArray = CHtml::listData($like, 'id', 'idea_id');

//        var_dump(array_search('12', $likesArray));
//
//        foreach ($data as $key => $value) {
//            (array_search('12', $likesArray) != false) ? $data[$key]->userLike = true : $data[$key]->userLike = false;
//        }
//        print_r($data);

        $this->render('index', array(
            'ideas' => $data,
            'userLikes' => $likesArray,
            'pages' => $pages
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Ideas the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Ideas::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Ideas $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ideas-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiondownloadfile()
    {
        $src = $_GET['attachment'];
        $original_name = $_GET['original_name'];
        $upload_path = Yii::getPathOfAlias('webroot') . '/attachments/';

        if (file_exists($upload_path . $src)) {
            Yii::app()->getRequest()->sendFile($original_name, file_get_contents($upload_path . $src));
        } else {
//            $this->render('download 404');
            return;
        }
    }
}
