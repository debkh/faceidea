<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'actions'=>array('view', 'index'),
                'users'=>array('*'),
            ),

            array('allow',
                'actions'=>array('edit','profile'),
                'users'=>array('@'),
            ),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionProfile()
	{
        $id = Yii::app()->user->id;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionIndex()
    {
        $model=new Users();
        $users=$model->findAll();
        $this->render('index',array(
            'model'=>$users,
        ));
    }


    public function actionEdit()
    {
        $id = Yii::app()->user->id;
        $model = $this->loadModel($id);
        $origdata = Users::model()->findByPk($id);

        if (isset($_POST['Users'])) {
            $userID = Yii::app()->user->id;
            $userName = Yii::app()->user->name;
            $model->attributes = $_POST['Users'];
            $uploadedFile=CUploadedFile::getInstance($model,'image');
            if ($model->password !== $origdata->password ){$model->password = sha1( $model->password );}
            $fileName = "{$userID}-{$userName}";  // random number + file name
            $fileName = md5($fileName);
            $model->image = $fileName.'.jpg';

            if(!empty($uploadedFile))  // check if uploaded file is set or not
            {
                $model->image = $fileName.'.jpg';
                $uploadedFile->saveAs(Yii::app()->basePath.'/../users_img/'.$fileName.'.jpg');
                $image = Yii::app()->image->load(Yii::app()->basePath.'/../users_img/'.$fileName.'.jpg');
                $image->quality(75);
                $image->resize(300,300);
                $image->crop(200, 200);
                $image->save(Yii::app()->basePath.'/../users_img/small/'.$fileName.'.jpg'); // or $image->save('images/small.jpg');
            }

            if ($model->save()) {
                $this->redirect(array('profile', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
