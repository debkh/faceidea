<?php
/* @var $this UsersController */
/* @var $model Users */

    $this->breadcrumbs=array(
	    'Profile'=>array('profile'),
    );
?>
<div class="postmain">
<h1> Users Profiles</h1>

<table id="user_list" class="table table-striped table-hover table-condensed">
    <thead>
    <tr>
        <td>
            <h5>Foto</h5>
        </td>
        <td>
            <h5>Name</h5>
        </td>
        <td>
            <h5>Email</h5>
        </td>
        <td>
            <h5>Specialization</h5>
        </td>
        <td>
            <h5>Office number</h5>
        </td>
    </tr>
    </thead>
    <tbody class="usertblz">
    <?php foreach($model as $uid):?>
        <tr>
            <td>
                <div style="height: 50px;">
                    <?php if (isset ($uid->image)) {
                        echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/small/'.$uid->image, "User foto", array('class'=>'img-thumbnail', 'width'=>'50'));}
                    else {
                        echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/default_user.png', "Default foto", array('class'=>'img-thumbnail', 'width'=>'50'));}
                    ?>
                </div>
            </td>

                <?php if(isset($uid->name) or isset($uid->email) or isset($uid->specialization) or isset($uid->office_number) ):?>
            <td>
                <a href='<?php echo Yii::app()->createUrl("/users/view/".$uid->id) ?>'><?php echo $uid->name;?></a>
            </td>
            <td>
                <?php echo $uid->email ?>
            </td>
            <td>
                <?php echo $uid->specialization ?>
            </td>
            <td>
                <?php echo $uid->office_number ?>
            </td>
                <?php else:?>
            <td colspan="4" style="vertical-align: middle; color:#959595">
                User has not filled their profile
            </td>
                <?php endif?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>


