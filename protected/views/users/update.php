<?php $this->breadcrumbs=array(
'Profile'=>array('profile') ,
'Edit'=>array('edit') ,
);?>
<div class="postmain clearfix">
    <header>
        <h1>Edit: <?php echo $model->name;?></h1>
    </header>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'users-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="form-group" style="padding-left: 15px">
        <?php if (isset ($model->image)) {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/small/'.$model->image, "User foto",array("width"=>200, 'class'=>'img-thumbnail'));}
        else {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/default_user.png', "Default foto",array("width"=>200, 'class'=>'img-thumbnail'));} ?>
    </div>
 <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'name', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-7">
                <?php echo $form->textField($model,'name',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50,'placeholder'=>'Name')); ?>
            </div>
            <?php echo $form->error($model,'name', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'email', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-7">
                <?php echo $form->emailField($model,'email',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50,'placeholder'=>'Email')); ?>
            </div>
            <?php echo $form->error($model,'email', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'Password', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-7">
                <?php echo $form->passwordField($model,'password',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50,'placeholder'=>'Password')); ?>
            </div>
            <?php echo $form->error($model,'password', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'image', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-7">
                <?php echo CHtml::activeFileField($model, 'image', array('class' => 'col-md-10')); ?>
            </div>
            <?php echo $form->error($model,'image', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>


    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'specialization', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-7">
                <?php echo $form->textfield($model,'specialization',array('class' => 'col-md-10 form-control','placeholder'=>'Specialization')); ?>
            </div>
            <?php echo $form->error($model,'specialization', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'office_number', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-2">
                <?php echo $form->dropDownList($model, 'office_number', array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                ), array('class' => 'col-md-10 form-control')) ?>
            </div>
        </div>
    </div>


<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
      <div class="row buttons col-md-2">
        <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary',) ); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

</div>