<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Profile'=>array('profile'),
);
?>
<div class="postmain clearfix">
<header>
    <h1>Profile: <?php echo $model->name;?></h1>
</header>
<div class="col-md-6 user">
    <div class="row">
        <?php if (isset ($model->image)) {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/small/'.$model->image, "User foto", array('class'=>'img-thumbnail'));}
        else {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/default_user.png', "Default foto", array('class'=>'img-thumbnail'));} ?>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Login:</strong></div>
        <div class="col-md-7"><?php echo $model->login; ?></div>
    </div>

    <?php if ($model->name): ?>
    <div class="row">
        <div class="col-md-5"><strong>Name:</strong></div>
        <div class="col-md-7"><?php echo $model->name; ?></div>
    </div>
    <?php endif ?>

    <?php if ($model->email): ?>
    <div class="row">
        <div class="col-md-5"><strong>Email:</strong></div>
        <div class="col-md-7"><?php echo $model->email; ?></div>
    </div>
    <?php endif ?>

    <?php if ($model->specialization): ?>
    <div class="row">
        <div class="col-md-5"><strong>Specialization:</strong></div>
        <div class="col-md-7"><?php echo $model->specialization; ?></div>
    </div>
    <?php endif ?>

    <?php if ($model->office_number): ?>
    <div class="row">
        <div class="col-md-5"><strong>Office number:</strong></div>
        <div class="col-md-7"><?php echo $model->office_number; ?></div>
    </div>
    <?php endif ?>

    <div class="row buttons col-md-3">
        <?php if(Yii::app()->user->getId() == $model->id){ ?>
            <a href="<?php echo Yii::app()->createUrl("/users/edit");?>" class="btn btn-primary">Edit</a>
        <?php } ?>
    </div>
</div>

<div class="col-md-6 row usercol">
    <?php if ($model->userIdeas): ?>
    <header><h4>Last 10 ideas</h4></header>
    <?php
        $dddd=0;
        foreach($model->userIdeas as $uid) {
        echo('<div class="row userideas" >');
        echo('<h4><a href="'.Yii::app()->createUrl("/ideas/".$uid->id).'" title="'.$uid->title.'">'.$uid->title.'</a></h4>');
        echo('<small>'.date('d M Y h:i a', strtotime($uid->date)).'</small>');
        if(strlen($uid->content) > 50) {
            $period = strpos($uid->content, '.', 50);
            $teaser = substr($uid->content, 0, $period);
            $teaser = strip_tags($teaser);

            echo('<div class="">'.$teaser.' ...</div>');
        }
        else{
            echo('<div class="">'.strip_tags($uid->content).'</div>');
        }
        echo('</div>');
            if (++$dddd == 10) break;
    } ?>
    <?php endif ?>
</div>
    </div>