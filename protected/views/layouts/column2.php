<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="col-md-2 nopaddingmarg" style="margin-bottom: 50px">
            <ul class="nav nav-pills nav-stacked">
            <li><a href="<?= Yii::app()->createUrl("/ideas/create");?>" class="list-group-item active">Create Idea</a></li>
            <li><a href="<?= Yii::app()->createUrl("/ideas/nulls");?>" class="list-group-item">Null Likes Idea</a></li>
            <li><a href="<?= Yii::app()->createUrl("/ideas/week");?>" class="list-group-item">Last Week Ideas</a></li>
            <li><a href="<?= Yii::app()->createUrl("/users/index");?>" class="list-group-item">All users</a></li>
        </ul>
    </div>
    <div class="col-md-10">
        <?php echo $content; ?>
    </div>
<?php $this->endContent(); ?>