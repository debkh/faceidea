<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/css/bootstrap.min.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/panel_idea.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/nicolas.css" media="screen, projection" />
    <link href='http://fonts.googleapis.com/css?family=Comfortaa|Philosopher|Roboto&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>
<!--    <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/bootstrap/js/bootswatch.js"></script>-->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/customscript.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main_idea.js"></script>
</head>

<body>
    <div class="navbar navbar-default navbar">
        <div class="container">
            <div class="navbar-header">
                <a href="<?php echo Yii::app()->homeUrl?>" >
                    <img class="navbar-brand" src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/img/logo.png" style="padding-left: 0px;" height="500">
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbar-main">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions' => array( 'class' => 'nav navbar-nav'),
                    'items'=>array(
                        array('label'=>'Home','class' => 'active', 'url'=>Yii::app()->homeUrl),
                       // array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                       // array('label'=>'Contact', 'url'=>array('/site/contact')),
                       // array('label'=>'Chat', 'url'=>array('/site/page', 'view'=>'chat'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Profile', 'url'=>array('/users/profile'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ))); ?>
            </div>
        </div>
    </div>

    <div class="container" id="page">
	    <?php if(isset($this->breadcrumbs)):?>
        <div class="row">
            <div id="bc1" class="btn-group btn-breadcrumb angles">
                <?php $this->widget('ext.MyBreadcrumb.MyCBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        )); ?>
            </div>
        </div>
        <?php endif?>

        <div class="row" style="margin-top: 20px;">
            <?php echo $content; ?>
        </div>

	    <div class="clear"></div>

        <div class="row">
            <div class="col-lg-12">
                <hr>
                <p><img src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/img/logo.png" height="20" style="padding-right: 20px;">Powered by <a href="http://faceit-team.com" rel="nofollow">FaceIT</a>.</p>
            </div>
        </div>




    </div><!-- page -->
</body>
</html>
