<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Enter';
?>


<div class="form form-horizontal">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'enter-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<h1 style="text-align: center; padding: 20px; font-size: 48px; ">Welcome on FaceIT Ideas</h1>
    <div class="form-group">
        <div class=" row col-lg-6 col-md-offset-4">
		    <?php //echo $form->labelEx($model,'login', array('class'=>'col-lg-3 control-label',)); ?>
            <div class="col-lg-7">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa-lg" style="width: 20px"></i></span>
		        <?php echo $form->textField($model,'login', array('class'=>'form-control', 'id'=>'inputDefault', 'placeholder'=>'Login')); ?>
                </div>
		    </div>
            <?php echo $form->error($model,'login', array('class'=>'bg-danger',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row col-lg-6 col-md-offset-4">
            <?php //echo $form->labelEx($model,'password', array('class'=>'col-lg-3 control-label', )); ?>
            <div class="col-lg-7">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-lg" style="width: 20px"></i></span>
                    <?php echo $form->passwordField($model,'password', array( 'class'=>'form-control', 'id'=>'inputEmail', 'placeholder'=>'Password')); ?>
                </div>
		    </div>
            <?php echo $form->error($model,'password', array('class'=>'bg-danger error',)); ?>
        </div>
    </div>

    <div class="form-group">
	    <div class=" row buttons col-lg-6 col-md-offset-4">
            <div class="col-lg-7" style="padding: 15px;">
                <?php echo CHtml::submitButton('Login', array( 'name' => 'Login', 'class' => 'col-lg-12 btn btn-primary', 'style' => 'width: 100%')); ?>
            </div>
	    </div>
        <div class=" row buttons col-lg-6 col-md-offset-4">
            <div class="col-lg-7">
                <?php echo CHtml::submitButton('Register', array( 'name' => 'Register', 'class' => 'col-lg-12 btn btn-success', 'style' => 'width: 100%')); ?>
            </div>
        </div>
    </div>

    <div class="bg-danger text-center ">
        <h2><?php echo $model->errorMessage; ?></h2>
    </div>

    <?php $this->endWidget(); ?>
</div>

<!-- form -->
