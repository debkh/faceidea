<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>
<div class="pagemain">
<header>
<h1 class="title">Contact Us</h1>
</header>
<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="form-group">
	<div class="row">
		<?php echo $form->labelEx($model,'name', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-7">
		<?php echo $form->textField($model,'name', array( 'class' => 'col-md-10 form-control', 'placeholder'=>'Name')); ?>
        </div>
		<?php echo $form->error($model,'name'); ?>
	</div>
    </div>

    <div class="form-group">
    <div class="row">
		<?php echo $form->labelEx($model,'email', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-7">
		<?php echo $form->textField($model,'email', array( 'class' => 'col-md-10 form-control', 'placeholder'=>'Email')); ?>
        </div>
		<?php echo $form->error($model,'email'); ?>
	</div>
    </div>

    <div class="form-group">
	<div class="row">
		<?php echo $form->labelEx($model,'subject', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-7">
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128,  'class' => 'col-md-10 form-control', 'placeholder'=>'Subject')); ?>
        </div>
		<?php echo $form->error($model,'subject'); ?>
	</div>
    </div>

    <div class="form-group">
	<div class="row">
		<?php echo $form->labelEx($model,'body', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-7">
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'class' => 'col-md-10 form-control','placeholder'=>'Your message...')); ?>
        </div>
		<?php echo $form->error($model,'body'); ?>
	</div>
    </div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row col-lg-12">
        <div class="row">
		<?php echo $form->labelEx($model,'verifyCode', array('class' => 'col-md-2',)); ?>

        <div class=" col-lg-4">
	    	<?php $this->widget('CCaptcha'); ?>
        </div>

        <div class=" col-lg-3">
    		<?php echo $form->textField($model,'verifyCode',array('class' => 'col-md-10 form-control', 'placeholder'=>'Enter code')); ?>
        </div>
        </div>
	</div>

        <div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
    <div>
        <?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="buttons">
		<?php echo CHtml::submitButton('Submit', array( 'class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>
    </div>