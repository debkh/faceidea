<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="form-group">
        <div class="row col-lg-6 col-md-offset-3">
		<?php echo $form->labelEx($model,'username', array('class'=>'col-lg-3 control-label',)); ?>
        <div class="col-lg-7">
        <?php echo $form->textField($model,'username', array('class'=>'form-control', 'id'=>'inputDefault',)); ?>
		</div>
        <?php echo $form->error($model,'username'); ?>
	</div>
    </div>

    <div class="form-group">
        <div class="row col-lg-6 col-md-offset-3">
		<?php echo $form->labelEx($model,'password', array('class'=>'col-lg-3 control-label',)); ?>
        <div class="col-lg-7">
		<?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'id'=>'inputDefault',)); ?>
        </div>
		<?php echo $form->error($model,'password'); ?>
	</div>
    </div>

    <div class="form-group">
        <div class="row col-lg-6 col-md-offset-3">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>
    </div>


    <div class="form-group">
        <div class=" row buttons col-lg-6 col-md-offset-3">
		<?php echo CHtml::submitButton('Login',  array( 'name' => 'Login', 'class' => 'col-lg-6 btn btn-primary',)); ?>
        <?php echo CHtml::submitButton('Register', array( 'name' => 'Register', 'class' => 'col-lg-6 btn btn-success',)); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
