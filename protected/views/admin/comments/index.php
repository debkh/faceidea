<?php
/* @var $this CommentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Comments',
);

$this->menu=array(
	array('label'=>'Create Comments', 'url'=>array('create')),
	array('label'=>'Manage Comments', 'url'=>array('admin')),
);
?>

<h1>Comments</h1>

<?foreach($models as $data):?>
    <div class="view">

        <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
        <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
        <?php echo CHtml::encode($data->date); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('idea_id')); ?>:</b>
        <?php echo CHtml::encode($data->idea->title); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
        <?php echo CHtml::encode($data->author->login); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
        <?php echo CHtml::encode($data->content); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('parent_comment_id')); ?>:</b>
        <?php echo CHtml::encode($data->parent_comment_id); ?>
        <br />

        <hr><hr>
    </div>
<?endforeach?>


<?$this->widget('CLinkPager', array(
    'pages' => $pages,
))?>