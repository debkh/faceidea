<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs=array(
	'Ideas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ideas', 'url'=>array('index')),
);
?>

<h1>Create Idea</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>