<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs=array(
	'Ideas'=>array('index'),
	$model->title,
);
?>

<h1><?php echo $model->title; ?></h1>

<p>by<?php echo $model->author->login; ?></p>
<p><?php echo($model->date)?></p>
<p><?php echo($model->content)?></p>

<?php if(isset($attachments)) : ?>
	<?php foreach($attachments as $key => $attachment) : ?>
		<h5><a  href="<?php echo $this->createUrl('downloadfile', array('attachment'=>$attachment->name, 'original_name'=>$attachment->original_name)) ;?>" target="helperFrame">
			Download <?php echo $attachment->original_name; ?>
		</a></h5>
	<?php endforeach ?>
<?php endif ?>
