<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs=array(
	'Ideas'=>array('index'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ideas-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form" style="float: right; padding: 0 2em">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>

<div class="container col-md-12">
    <div class="row">
        <div class="col-md-12">

<!--            --><?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--            <div class="search-form" style="display:none">-->
<!--            --><?php //$this->renderPartial('_search',array(
//                'model'=>$model,
//            )); ?>
<!--            </div><!-- search-form -->

            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'ideas-grid',
                'dataProvider'=>$model->search(),
//                'filter'=>$model,
                'itemsCssClass' => 'table table-striped table-hover',
                'htmlOptions' => array(
                    'class'=>'table table-boredred table-hover'
                ),
                'columns'=>array(
                    'title',
                    'date',
                    'author.login:text:Author',
                    'likeCount:text:Likes',
                    'attachmentsCount:text:Attachments',
                    array(
                        'name'=>'status_confirmed',
                        'header'=>'Confirmed',
                        'filter'=>array('1'=>'Yes','0'=>'No'),
                        'value'=>'($data->status_confirmed=="1")?("Yes"):("No")'
                    ),
                    array(
                        'header'=>'Edit',
                        'class'=>'CButtonColumn',
                        //If you don`t need custom buttons comment this
                        /*'template'=>'{email}{down}{delete}',
                        'buttons'=>array
                        (
                            'email' => array
                            (
                                'label'=>'Send an e-mail to this user',
                                'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
                                'url'=>'Yii::app()->createUrl("users/email", array("id"=>$data->id))',
                            ),
                            'down' => array
                            (
                                'label'=>'[-]',
                                'url'=>'"#"',
                                'visible'=>'$data->score > 0',
                                'click'=>'function(){alert("Going down!");}',
                            ),
                        ),*/
                        // End custom buttons
                        'buttons'=>array(
                            'view'=>array(
                                'label'=>'<i class="fa fa-eye"></i>',
                                'imageUrl'=>false,
                            ),

                            'update'=>array(
                                'label'=>'<i class="fa fa-pencil-square"></i>',
                                'imageUrl'=>false,
                            ),

                            'delete'=>array(
                                'label'=>'<i class="fa fa-trash-o"></i>',
                                'imageUrl'=>false,
                            ),

                        ),
                    ),
                ),
            )); ?>
        </div>
    </div>
</div>