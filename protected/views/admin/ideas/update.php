<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs=array(
	'Ideas'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Ideas', 'url'=>array('create')),
	array('label'=>'View Ideas', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1>Update Ideas <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

