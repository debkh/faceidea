<?php
/* @var $this IdeasController */
/* @var $model Ideas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ideas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>
    <div class="bg-danger text-center ">
	<?php echo $form->errorSummary($model); ?>
    </div>
<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'date'); ?>
<!--		--><?php //echo $form->textField($model,'date'); ?>
<!--		--><?php //echo $form->error($model,'date'); ?>
<!--	</div>-->

<!--    <h3>--><?php //echo("by " . Yii::app()->user->getId()) ; ?><!--</h3>-->

    <div class="form-group">
	<div class="row">
		<?php echo $form->labelEx($model,'title', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		    <?php echo $form->textField($model,'title',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50)); ?>
		</div>
        <?php echo $form->error($model,'title', array('class' => 'warning',)); ?>
	</div>
        </div>
    <div class="form-group">
	<div class="row">
		<?php echo $form->labelEx($model,'content', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
            <?php
            $attribute='content';
            $this->widget('application.widgets.redactorjs.Redactor', array(
                'model'=>$model,
                'attribute'=>$attribute,
            ));
            ?>
        </div>
        <?php echo $form->error($model,'content', array('class' => 'warning',)); ?>
	</div>
    </div>

    <div class="form-group">
    <div class="row">
    <?php
    $this->widget('CMultiFileUpload', array(
        'name'=>'attachments',
        'model'=>$model,
        'attribute'=>'files',
        'htmlOptions' => array('class'=>'col-md-12'),
        'accept'=>'jpg|jpeg|bmp|png|txt|doc|docx|odt|pdf|sql|zip|rar',
        'denied'=>'Only jpg, doc, docx, pdf and txt are allowed',
        'duplicate'=>'Already Selected',
        'options'=>array(
            'onFileSelect'=>'function(e, v, m){}',
            'afterFileSelect'=>'function(e, v, m){}',
            'onFileAppend'=>'function(e, v, m){}',
            'afterFileAppend'=>'function(e, v, m){}',
            'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
            'afterFileRemove'=>'function(e, v, m){ }',
        ),
    ));
    ?>
    </div>
    </div>

    <div class="row buttons col-md-3">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary',) ); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
