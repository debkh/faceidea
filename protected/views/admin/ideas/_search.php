<?php
/* @var $this IdeasController */
/* @var $model Ideas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<form class="form-inline" role="form">
    <div class="form-group">
        <?php echo $form->textField($model,'title',array('size'=>25,'placeholder'=>'Title','maxlength'=>25,'class'=>'form-control',)); ?>
    </div>
    <?php echo CHtml::submitButton('Search',array( 'class' => 'btn btn-primary',)); ?>
</form>