<?php
/* @var $this DashboardController */

$this->breadcrumbs=array(
	'Dashboard',
);
?>


<div id="ideasCharts">
	<!-- Display ideas counts by months -->
	<div id="ideasByMonths" class="dashboard-tbl">
	</div>
	<span id="ideasByMonthsValues" hidden>
	<?php
		foreach( $ideasByMonths as $item )
			echo $item->c . '-' . $item->m . ',';
	?>
	</span>

	<!-- Display ideas counts by approved -->
	<div id="ideasByStatus" class="dashboard-tbl">
	</div>
	<span id="ideasByStatusValues" hidden>
	<?php
		foreach( $ideasByStatus as $item )
			echo $item->c . '-' . $item->status_confirmed . ',';
	?>
	</span>
</div>

<?php if( isset( $users ) && !empty( $users ) ) :?>
		<div id="UnconfirmedUses" class="dashboard-tbl">
				<h3 style="margin:0 0 10px -5px">New Users</h3>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Login</th>
                    <th>User Name</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
		<?php foreach( $users as $user ) : ?>
            <tr class="<?php echo 'userid'.$user->id ?>" class="dashboard-tbl-user">
                <td style="width: 5%"><?php echo $user->id ?></td>
                <td style="width: 15%"><?php echo $user->login ?></td>
                <td style="width: 63%"><?php echo $user->name ?></td>
                <td id="<?php echo $user->id ?>">
                    <a class="confirmUser btn btn-success" href="#"><i class="fa fa-check-circle fa-lg"></i></a>
                    <a class="unconfirmUser btn btn-danger" href="#"><i class="fa fa-trash-o fa-lg"></i> </a>
                </td>
        <?php endforeach; ?>
            </table>
</div>
	<?php endif?>

