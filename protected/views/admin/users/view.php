<?php

$this->breadcrumbs=array(
    'Users'=>array('index'),
);

?>

<h1><?php echo $model->name;?></h1>
<div class="row">
    <div class="col-md-3 user" style="padding-left: 80px;">
        <?php if (isset ($model->image)) {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/small/'.$model->image, "User foto", array('class'=>'img-thumbnail'));}
        else {
            echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/default_user.png', "Default foto", array('class'=>'img-thumbnail'));} ?>
    </div>
    <div class="col-md-7  user">
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'htmlOptions'=>array('class'=>'table table-hover'),
            'attributes'=>array(
                'login',
                'roleName.name:text:Role',
                'name',
                'email',
                'specialization',
                'office_number',
                'status_confirmed'=>array(
                    'name'=>'Confirmed',
                    'value'=>$model->status_confirmed==1?'Yes':'No'
                ),
                'ban'=>array(
                    'name'=>'Ban',
                    'value'=>$model->ban==1?'Yes':'No'
                ),
            ),
        ));
        ?>


    </div>
    <div class="row buttons col-md-3">
        <a href="update/<?php echo $model->id?>" class="btn btn-primary">Edit</a>
    </div>
</div>
