<?php $this->breadcrumbs=array(
    'Profile'=>array('profile') ,
    'Edit'=>array('edit') ,

);?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'users-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="form-group">

        <?php if($model->isNewRecord!='1'){ ?>

            <?php echo CHtml::image(Yii::app()->request->baseUrl.'/users_img/'.$model->image,"image",array("width"=>200)); ?>

        <?php }?>

    </div>
    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'id', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $model->id    ?>
            </div>
            <?php echo $form->error($model,'id', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'login', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->textField($model,'login',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50)); ?>
            </div>
            <?php echo $form->error($model,'login', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'name', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->textField($model,'name',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50)); ?>
            </div>
            <?php echo $form->error($model,'name', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'email', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->textField($model,'email',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50)); ?>
            </div>
            <?php echo $form->error($model,'email', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'password', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->passwordField($model,'password',array('class' => 'col-md-10 form-control', 'id'=>'inputDefault','size'=>50,'maxlength'=>50)); ?>
            </div>
            <?php echo $form->error($model,'password', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'image', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo CHtml::activeFileField($model, 'image', array('class' => 'col-md-10')); ?>
            </div>
            <?php echo $form->error($model,'image', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>


    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'specialization', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->textfield($model,'specialization',array('class' => 'col-md-10 form-control')); ?>
            </div>
            <?php echo $form->error($model,'specialization', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'office_number', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->dropDownList($model, 'office_number', array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                ), array('class' => 'col-md-10 form-control')) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'role', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->radioButtonList($model,'role',array(
                    '1' => 'User',
                    '2' => 'Moderator',
                    '3' => 'Administrator',
                ), array('class' => 'col-md-10 form-control')); ?>
            </div>
            <?php echo $form->error($model,'role', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'status_confirmed', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->checkBox($model,'status_confirmed',array('class' => 'col-md-10 form-control')); ?>
            </div>
            <?php echo $form->error($model,'status_confirmed', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <?php echo $form->labelEx($model,'ban', array('class' => 'col-md-2',)); ?>
            <div class="col-lg-10">
                <?php echo $form->checkBox($model,'ban',array('class' => 'col-md-10 form-control')); ?>
            </div>
            <?php echo $form->error($model,'ban', array('class' => 'btn btn-primary',)); ?>
        </div>
    </div>

    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
    <div class="row buttons col-md-3">
        <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary',) ); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

