<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>



<?php $this->endWidget(); ?>

</div><!-- search-form -->
<form class="form-inline" role="form">
    <div class="form-group">
        <?php echo $form->textField($model,'login',array('size'=>25,'placeholder'=>'Login','maxlength'=>25,'class'=>'form-control',)); ?>
    </div>
    <div class="form-group">
        <div class="input-group">
            <?php echo $form->textField($model,'name',array('size'=>25,'placeholder'=>'User name','maxlength'=>50,'class'=>'form-control',)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->textField($model,'email',array('size'=>25,'placeholder'=>'E-mail','maxlength'=>50,'class'=>'form-control',)); ?>
    </div>
    <?php echo CHtml::submitButton('Search',array( 'class' => 'btn btn-primary',)); ?>
</form>