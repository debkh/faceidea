<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs = array(
    'Ideas' => array('index'),
    $model->title,
);
?>
<article class="article">
    <div id="content_box ">
        <!--single_post start-->
        <div class="single_post postmain">
            <header>
                <h1 class="title single-title"><?php echo $model->title; ?></h1>

                <div class="post-info">
                    <span class="thetime">Date: <strong><?php echo date('d M Y', strtotime($model->date)); ?></strong></span>  |
                    <span class="theauthor">Author: <strong><a rel="nofollow"
                                                       href="<?= Yii::app()->createUrl("/users/view/" . $model->user_id . "") ?>"
                                                       title="Posts by <?php echo $model->author->name; ?>"><?php echo $model->author->name; ?></a></strong></span>
                </div>
            </header>
            <!--.headline_area-->
            <div class="post-single-content box mark-links">
                <?php echo($model->content) ?>

                <?php if (isset($attachments)) : ?>
                    <?php foreach ($attachments as $key => $attachment) : ?>
                        <h5>
                            <a href="<?php echo $this->createUrl('downloadfile', array('attachment' => $attachment->name, 'original_name' => $attachment->original_name)); ?>"
                               target="helperFrame">
                                Download <?php echo $attachment->original_name; ?>
                            </a></h5>
                    <?php endforeach ?>
                <?php endif ?>
                <!--            <div class="tags">-->
                <!--                <span class="tagtext">Tags:</span>-->
                <!--                <a href="http://demo.mythemeshop.com/dualshock/tag/qiameth/" rel="tag">Qiameth</a>, <a href="http://demo.mythemeshop.com/dualshock/tag/tupress/" rel="tag">Tupress</a>, <a href="http://demo.mythemeshop.com/dualshock/tag/winooze/" rel="tag">Winooze</a>-->
                <!--            </div>-->
            </div>

            <div id="comments">
                <div class="total-comments"><?= count($comments); ?> Comments</div>
                <ol class="commentlist">
                    <?php
                    if (isset($comments)) {
                        foreach ($comments as $comment) {
                            if ($comment->parent_comment_id == 0) {
                                ?>
                                <li class="comment odd alt thread-even depth-1 lino3"
                                    id="li-comment-<?= $comment->id ?>">
                                    <div id="comment-<?= $comment->id ?>" style="position:relative;">
                                        <div class="comment-author vcard clearfix">
                                            <div class="comment-image">
                                            <?php if (isset ($comment->author->image)) {
                                                echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/small/' . $comment->author->image, "User foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                            } else {
                                                echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/default_user.png', "Default foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                            } ?>
                                            </div>

                                            <span class="comment-name"><?php echo $comment->author->name ?></span>
                                            <time><?php echo date('d M Y \a\t g:s a', strtotime($comment->date)); ?></time>

                                        </div>
                                        <div class="comment-meta"></div>
                                        <div class="commentmetadata">
                                            <?php echo $comment->content; ?>
                                        </div>
                                        <div class="reply">
                                            <a class="comment-reply-link" href="?replytocom=<?= $comment->id ?>#respond"
                                               onclick=" return addComment.moveForm(&quot;comment-<?= $comment->id ?>&quot;, &quot;<?= $comment->id ?>&quot;, &quot;respond&quot;, &quot;<?= $model->id ?>&quot;)"
                                               rel="nofollow">Reply</a>
                                        </div>
                                    </div>
                                </li>
                                <?php $childe_comments = (Comments::model()->findAll('parent_comment_id = ' . $comment->id));
                                if (!empty($childe_comments)) {
                                    foreach ($childe_comments as $ch_com): ?>


                                        <ul class="children">
                                            <li class="comment odd alt thread-even depth-2 lino3"
                                                id="li-comment-<?= $comment->id ?>">
                                                <div id="comment-<?= $ch_com->id ?>" style="position:relative;">
                                                    <div class="comment-author vcard clearfix">
                                                        <div class="comment-image">
                                                        <?php if (isset ($ch_com->author->image)) {
                                                            echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/small/' . $ch_com->author->image, "User foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                                        } else {
                                                            echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/default_user.png', "Default foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                                        } ?>
                                                        </div>
                                                        <span class="comment-name"><?php echo $ch_com->author->name ?></span>
                                                        <time><?php echo date('d M Y \a\t g:s a', strtotime($ch_com->date)); ?></time>
                                                    </div>
                                                    <div class="comment-meta"></div>
                                                    <div class="commentmetadata">
                                                        <?php echo $ch_com->content; ?>
                                                    </div>
                                                    <div class="reply">
                                                        <a class="comment-reply-link"
                                                           href="?replytocom=<?= $ch_com->id ?>#respond"
                                                           onclick="document.getElementById('leave_r').style.display='block';return addComment.moveForm(&quot;comment-<?= $ch_com->id ?>&quot;, &quot;<?= $ch_com->id ?>&quot;, &quot;respond&quot;, &quot;<?= $model->id ?>&quot;)"
                                                           rel="nofollow">Reply</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <!--comment-## -->
                                        </ul>
                                        <?php $childe2_comments = (Comments::model()->findAll('parent_comment_id = ' . $ch_com->id));
                                        if (!empty($childe2_comments)) {
                                            foreach ($childe2_comments as $ch_com2):?>
                                                <ul class="children">
                                                    <li class="comment odd alt thread-even depth-2 lino3"
                                                        id="li-comment-<?php echo $comment->id ?>">
                                                        <div id="comment-<?php echo $ch_com2->id ?>"
                                                             style="position:relative;">
                                                            <div class="comment-author vcard clearfix">
                                                                <div class="comment-image">
                                                                <?php if (isset ($ch_com2->author->image)) {
                                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/small/' . $ch_com2->author->image, "User foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                                                } else {
                                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/default_user.png', "Default foto", array('class' => 'img-thumbnail', 'width' => '70'));
                                                                } ?>
                                                                </div>
                                                                <span class="comment-name"><?php echo( $ch_com2->author->name.' to '.$ch_com->author->name); ?></span>
                                                                <time><?php echo date('d M Y \a\t g:s a', strtotime($ch_com2->date)); ?></time>
                                                            </div>
                                                            <div class="comment-meta"></div>
                                                            <div class="commentmetadata">
                                                                <?php echo $ch_com2->content; ?>
                                                            </div>
                                                            <div class="reply">
                                                                <a class="comment-reply-link"
                                                                   href="?replytocom=<?= $ch_com2->id ?>#respond"
                                                                   onclick="document.getElementById('leave_r').style.display='block';return addComment.moveForm(&quot;comment-<?= $ch_com2->id ?>&quot;, &quot;<?= $ch_com2->id ?>&quot;, &quot;respond&quot;, &quot;<?= $model->id ?>&quot;)"
                                                                   rel="nofollow">Reply</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <!--comment-## -->
                                                </ul>
                                            <?php endforeach;
                                        }  endforeach;
                                }
                            }
                        }
                    }; ?>
                </ol>
            </div>

            <div id="commentsAdd">
                <div id="respond" class="box m-t-6">
                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title"></h3>
                        <h4><span>Leave a Reply</span></h4>
                        <?php if (isset($_GET['replytocom'])) echo '<h6 style="display: block" id="leave_r"><a href="javascript:void(0)" onclick="document.getElementById(\'comment_parent\').value=\'0\'; document.getElementById(\'leave_r\').style.display=\'none\'" style="text-align: right; color: #ff0000; display: block;"><span>Cancel reply</span></a></h6>' ?>


                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'commentform',
                            'enableAjaxValidation' => false,
                        )); ?>
                        <p class="comment-form-comment">
                            <?php
                            echo $form->textArea(new Comments, 'content', array('class' => 'form-control', 'id' => 'comment'));
                            ?>
                        </p>

                        <p class="form-submit">
                            <div class="buttons">
                                <?php echo CHtml::submitButton('Post Comment', array('name' => 'Reply', 'class' => 'btn btn-primary', 'id' => 'submit')); ?>
                            </div>
                            <input type="hidden" name="comment_post_ID" value="<?= $model->id ?>" id="comment_post_ID">
                            <input type="hidden" name="comment_parent" id="comment_parent"
                                   value="<?php if (isset($_GET['replytocom'])) {
                                       echo $_GET['replytocom'];
                                   } else {
                                       echo 0;
                                   }; ?>">
                        </p>

                        <?php
                        $this->endWidget();
                        ?>
                    </div>
                    <!-- #respond -->
                </div>
            </div>
        </div>
    </div>
</article>