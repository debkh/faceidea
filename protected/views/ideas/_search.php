<?php
/* @var $this IdeasController */
/* @var $model Ideas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'id', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'id', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'date', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'date', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'user_id', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'user_id', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'title', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50, 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'content', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'like_count', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'like_count', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'dislike_count', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
		<?php echo $form->textField($model,'dislike_count', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row">
		<?php echo $form->label($model,'status_confirmed', array('class' => 'col-md-2',)); ?>
        <div class="col-lg-10">
	    <?php echo $form->textField($model,'status_confirmed', array( 'class' => 'col-md-10 form-control')); ?>
        </div>
	</div>
    </div>


    <div class="form-group">
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search', array( 'class' => 'btn btn-primary')); ?>
	</div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->