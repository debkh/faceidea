<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Enter';
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'enter-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
        <div class=" row col-lg-6 col-md-offset-3">
		<?php echo $form->labelEx($model,'login', array('class'=>'col-lg-3 control-label',)); ?>
		<?php echo $form->textField($model,'login', array('class'=>'form-control', 'id'=>'inputDefault',)); ?>
		<?php echo $form->error($model,'login', array('class'=>'bg-danger',)); ?>
        </div>

        <div class="form-group">
        <div class="row col-lg-6 col-md-offset-3">
        <?php echo $form->labelEx($model,'password', array('class'=>'col-lg-3 control-label', )); ?>
		<?php echo $form->passwordField($model,'password', array( 'class'=>'form-control', 'id'=>'inputEmail', )); ?>
		<?php echo $form->error($model,'password', array('class'=>'bg-danger',)); ?>
        </div>
        </div>

        <div class="form-group">
	    <div class=" row buttons col-lg-6 col-md-offset-3">
		<?php echo CHtml::submitButton('Register', array( 'name' => 'Register', 'class' => 'btn btn-success',)); ?>
		<?php echo CHtml::submitButton('Login', array( 'name' => 'Login', 'class' => 'btn btn-primary',)); ?>
	    </div>
        </div>
	<?php echo $model->errorMessage; ?>

<?php $this->endWidget(); ?>
</div>
       <!-- </fieldset>
</form><!-- form -->
