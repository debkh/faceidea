<?php
/* @var $this IdeasController */
/* @var $data Ideas */
?>

<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('like_count')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->like_count); ?>
<!--	<br />-->
<!---->
<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('dislike_count')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->dislike_count); ?>
<!--	<br />-->

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status_confirmed')); ?>:</b>
	<?php echo CHtml::encode($data->status_confirmed); ?>
	<br />

	*/ ?>

</div>