<?php
/* @var $this IdeasController */
/* @var $model Ideas */

$this->breadcrumbs=array(
	'Ideas'=>array('index'),
	'Create',
);
?>
    <div id="content_box" >
        <div class="postmain clearfix">
            <header><h1 class="title">Create Idea</h1></header>
            <?php $this->renderPartial('_form', array('model'=>$model)); ?>
        </div>
    </div>