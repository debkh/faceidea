<?php
/* @var $this IdeasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Ideas',
);
?>

<div id="content_box">
    <?php if ($ideas) : ?>
    <?php foreach ($ideas as $key => $idea) : ?>
        <div style="position: relative; top: -35px; left: 70px; z-index: 100; float: left">
            <?php if (isset ($idea->author->image)) {
                echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/small/' . $idea->author->image, "User foto", array('class' => 'img-thumbnail', 'width' => '100'));
            } else {
                echo CHtml::image(Yii::app()->request->baseUrl . '/users_img/default_user.png', "Default foto", array('class' => 'img-thumbnail', 'width' => '100'));
            }
            ?></div>
        <div class="postmain">

            <header>
                <h2 class="title">
                    <a href="<?php echo Yii::app()->createUrl("/ideas/" . $idea->id); ?>" title="<?= $idea->title; ?>"
                       rel="bookmark"><?= $idea->title; ?></a>
                </h2>
            </header>
            <!--.header-->
            <?php
            //$post_bez_br = preg_replace("'<p>'", '', $idea->content);
            //$post_bez_br = preg_replace("'</p>'", ' ', $post_bez_br);
            //$post_bez_br = preg_replace("'<br>'", ' ', $post_bez_br);

            $post_bez_br = strip_tags($idea->content);
            if (strlen($post_bez_br) > 100) {
                $post_content_short = mb_substr($post_bez_br, 0, 500);
                $post_content_short = $post_content_short . '...';
            } else {
                $post_content_short = $post_bez_br;
            }
            ?>
            <div class="post-content"><p><?= $post_content_short; ?></p></div>
            <hr style="border-color: #CCC;">
            <span class="thetime"><strong><?php echo date('d M Y', strtotime($idea->date)); ?></strong> |</span>
            <span class="theauthor"><a rel="nofollow"
                                       href="<?= Yii::app()->createUrl("/users/view/" . $idea->user_id . "") ?>"
                                       title="Idea by <? echo $idea->author->name ?>"
                                       rel="author"><? echo $idea->author->name ?></a> |</span>
            <span class="thecomment"><span class="comments">Comments</span> <span
                    class="comm"><strong><?= $idea->likeComment; ?></strong> |</span></span>
            <span class="readMore"><strong><a href="<?php echo Yii::app()->createUrl("/ideas/" . $idea->id); ?>"
                                              title="<?= $idea->title; ?>" rel="bookmark">Read More</a></strong></span>
            <button data-idea-id='<?= $idea->id; ?>_<?= Yii::app()->user->id; ?>' type="button" class="like pull-right"
                    style="background: none repeat scroll 0% 0% transparent; border: 0px none;">
                <i style="font-size: 1.8em; color: green;" class="fa fa-thumbs-o-up <?php if (array_search($idea->id, $userLikes) != false) echo "show-fade"; ?>"></i>
                <i style="font-size: 1.8em; color: #ff0000" class="fa fa-thumbs-o-down <?php if (array_search($idea->id, $userLikes) == false) echo "show-fade"; ?>"></i>
                <span style="position: relative; top: -5px; left: 7px; font-size: 1em;" class="label label-primary"><?= $idea->likeCount; ?></span>
            </button>
        </div>
    <?php endforeach; ?>

    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
        'nextPageLabel' => 'Next',
        'prevPageLabel' => 'Prev',
        'htmlOptions' => array(
            'class' => 'pagination',
            'style' => 'margin:0 0 0 40px;',
        ))) ?>

</div>

<?php else: ?>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1>You can be the first in FaceIT Ideas and your idea will bee first on your project.</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <a href="<?= Yii::app()->createUrl("/ideas/create");?>" class="list-group-item active" style="text-align: center; margin-top: 35px">Create Idea</a>
        </div>
    </div>

<?php endif; ?>

