<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property integer $id
 * @property integer $role
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $specialization
 * @property integer $office_number
 * @property string $image
 * @property integer $status_confirmed
 * @property integer $ban
 */
class Users extends CActiveRecord
{
	public $errorMessage;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('role, login, password, status_confirmed', 'required'),
			array( 'login', 'unique' ),
			array('role, office_number, status_confirmed, ban', 'numerical', 'integerOnly'=>true),
			array('login, specialization', 'length', 'max'=>25),
			array('password', 'length', 'max'=>40),
			array('name, email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, role, roleName.name, login, password, name, email, specialization, office_number, image, status_confirmed, ban', 'safe', 'on'=>'search'),
            array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'insert,update'), // this will allow empty field when page is update (remember here i create scenario update)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'roleName' => array(self::BELONGS_TO, 'Roles', 'role'),
            'userIdeas' => array(self::HAS_MANY, 'Ideas', 'user_id', 'order'=>'id DESC')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'role' => 'Role',
			'login' => 'Login',
			'password' => 'Password',
			'name' => 'Name',
			'email' => 'Email',
			'specialization' => 'Specialization',
			'office_number' => 'Office Number',
			'image' => 'Image',
			'status_confirmed' => 'Status Confirmed',
			'ban' => 'Ban',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *   ккк
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('role',$this->role);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('specialization',$this->specialization,true);
		$criteria->compare('office_number',$this->office_number);
		/*$criteria->compare('image',$this->image,true);*/
		$criteria->compare('status_confirmed',$this->status_confirmed);
		$criteria->compare('ban',$this->ban);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function registration( $user ) {
		$this->login = $user['login'];
		$this->password = sha1( $user['password'] );
		$this->status_confirmed = 0;
		$this->role = 1;
		if ( $this->validate() )
			return $this->save();
		else {
			return false;
		}
	}

	public function login() {
		$auth = new UserIdentity( $_POST['Users']['login'], $_POST['Users']['password'] );
		if ( $auth->authenticate() ) {
			Yii::app()->user->login( $auth, 3600*24*30 );
			return true;
		} else {
			$this->errorMessage = $auth->errorCode == 1 ? 'Invalid Username' : 'Invalid Password';
			return false;
		}
	}

}
