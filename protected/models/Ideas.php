<?php

/**
 * This is the model class for table "tbl_ideas".
 *
 * The followings are the available columns in table 'tbl_ideas':
 * @property integer $id
 * @property string $date
 * @property integer $user_id
 * @property string $title
 * @property string $content
 * @property integer $like_count
 * @property integer $dislike_count
 * @property integer $status_confirmed
 */
class Ideas extends CActiveRecord
{
	public $c, $m;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_ideas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content', 'required'),
			array('user_id, status_confirmed', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, user_id, title, content, status_confirmed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'author' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'attachmentsCount' => array(self::STAT, 'Attachments', 'id'),
            'likeCount' => array(self::STAT, 'Likes', 'idea_id'),
            'likeComment' => array(self::STAT, 'Comments', 'idea_id'),
            'relatedLikes' => array(self::HAS_MANY, 'Likes', 'idea_id'),
//            'isLiked' => array(self::HAS_ONE, 'Likes', 'idea_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'user_id' => 'User',
			'title' => 'Title',
			'content' => 'Content',
			'status_confirmed' => 'Status Confirmed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status_confirmed',$this->status_confirmed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ideas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getComments() {
		return Comments::model()->findAllByAttributes( array( 'idea_id' => $this->id ) );
	}
}
