<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    static private $_user;
    static private $_users=array();
    static private $_userByName=array();
    static private $_admin;
    static private $_admins;
    private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $username=strtolower($this->username);
		$user = Users::model()->findByAttributes( array( 'login' => $username ) );

        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if( $user->password != sha1( $this->password ) )
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$user->id;
            $this->username=$user->login;
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
	}

    public function getId()
    {
        return $this->_id;
    }

    /**
     * Return admin status.
     * @return boolean
     */
//    public static function isAdmin() {
//        if(Yii::app()->user->isGuest)
//            return false;
//        else {
//            if (!isset(self::$_admin)) {
//                if(self::user()->superuser)
//                    self::$_admin = true;
//                else
//                    self::$_admin = false;
//            }
//            return self::$_admin;
//        }
//    }
    /**
     * Return safe user data.
     * @param user id not required
     * @return user object or false
     */
    public static function user($id=0,$clearCache=false) {
        if (!$id&&!Yii::app()->user->isGuest)
            $id = Yii::app()->user->id;
        if ($id) {
            if (!isset(self::$_users[$id])||$clearCache)
                self::$_users[$id] = Users::model()->findbyPk($id);
            return self::$_users[$id];
        } else return false;
    }
}