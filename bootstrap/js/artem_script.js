$(document).ready( function() {

//------------------------------------------------------------------------------------------------------------

	$( '.commentAuthor' ).click( function() {
		$( '#Comments_content' ).focus().val( '@' + $(this).text() + ', ' );
	});

	$( 'a.delete' ).click( function(e) {
		e.preventDefault();
		if ( $( e.target ).hasClass( 'redirect' ) && !confirm( 'Are you sure you want to delete this user?' ) )
			return false;
		var href = $( this ).attr( 'href' ).split( '/' );
		var data = {
			'id' : href[href.length - 1]
		};
		$.post( 'delete', data, function( resp ) {
			if ( $( e.target ).hasClass( 'redirect' ) )
				window.location.pathname = '/admin/users/';
			else
			   $( e.target ).parents( 'tr' ).remove();
		} );
	});

//------------------------------------------------------------------------------------------------------------

    $( '.confirmUser' ).click( function(e) {
		e.preventDefault();
		var url = window.location.origin + '/admin/users/confirmation';
		var data = {
			'id' : $( this ).parent('td').attr('id')
		};
        var line = '.userid' + $( this ).parent('td').attr('id');
		$.post( url, data, function( resp ) {
			if ( resp == 'Success' )
                $( line ).fadeOut();
		} );
	})

//------------------------------------------------------------------------------------------------------------

    $( '.unconfirmUser' ).click( function(e) {
        e.preventDefault();
        var url = window.location.origin + '/admin/users/unconfirm';
        var data = {
            'id' : $( this ).parent('td').attr('id')
        };
        var line = '.userid' + $( this ).parent('td').attr('id');
        $.post( url, data, function( resp1 ) {
            if ( resp1 == 'Success' )
                $( line ).fadeOut();
        } );
    })

//------------------------------------------------------------------------------------------------------------

	if ( window.location.pathname.indexOf( 'admin/' ) > -1 ) { //there loading charts
        if (document.getElementById("ideasByMonthsValues")){
		var tmp = $( '#ideasByMonthsValues' ).html().split(',');
		tmp.pop();
		//1st Chart
		var workValues1 = [];
		for( var i = 0; i < tmp.length; i++ )
			workValues1[tmp[i].split('-')[1]] = parseInt( tmp[i].split('-')[0] );
		for( var i =-0; i < 12; i++ )
			if ( typeof workValues1[i] === 'undefined' )
				workValues1[i] = 0;

		setTimeout( function() {
			var options1 = {
				chart: {
					renderTo : 'ideasByMonths',
					type: 'column'
				},
				title: {
					text: 'Ideas Stats'
				},
				xAxis: {
					categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
				},
				yAxis: {
					title: {
						text: 'Count'
					}
				},
				series: [{
					name: new Date().getFullYear(),
					data: workValues1
				}]
			}
			var chart1 = new Highcharts.Chart(options1);
		}, 0);

		//2nd Chart
		var tmpstr = $( '#ideasByStatusValues' ).html().replace( '-1', '-Considered' )
														.replace( '-2', '-Rejected' )
														.replace( '-3', '-Approved' );
		var tmp = tmpstr.split(',');
		tmp.pop();
		var workValues2 = [];
		for( var i = 0; i < tmp.length; i++ )
			workValues2[i] = [ tmp[i].split('-')[1], parseInt( tmp[i].split('-')[0] ) ];

		setTimeout( function() {
			var options2 = {
				chart: {
					renderTo : 'ideasByStatus',
					plotBackgroundColor: null,
					plotBorderWidth: 1,//null,
					plotShadow: false
				},
				title: {
					text: 'Status Chart'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					type: 'pie',
					name: 'Count',
					data:workValues2
				}]
			}
			var chart1 = new Highcharts.Chart(options2);
		}, 0);
        }
	}
})