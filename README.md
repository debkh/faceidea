# FaceIdea #

FaceIdea проект на Yii для сбора идей сотрудников для создания на их основе стартапов.

## My SQL dump ##

Дамп базы данных находится **data/FaceIdeas.sql**

## Подключение к базе данных ##


***protected/config/console_sample.php*** скопировать и переименовать в ***console.php***

***protected/config/main_sample.php*** скопировать и переименовать в ***main.php***

---

В файле ***main.php***

```
#!php
'db'=>array(
'connectionString' => 'mysql:host=имя_сервера;dbname=имя_базы',
'emulatePrepare' => true,
'username' => 'имя_пользователя',
'password' => 'пароль',
'charset' => 'utf8',
),
```

---

В файле ***console.php***
```
#!php
'db'=>array(
'connectionString' => 'mysql:host=имя_сервера;dbname=имя_базы',
'emulatePrepare' => true,
'username' => 'имя_пользователя',
'password' => 'пароль',
'charset' => 'utf8',
),
```


## Миграция Yii ##
 Yii есть поддержка миграций, позволяющая отслеживать изменения в базе данных, применять миграции или откатывать уже применённые.

 * в консоли перейти в папку проекта (c помощью команды **cd**)
 * выполнить от имени пользоавтеля $ **./protected/yiic migrate**
 
---

Скачать **Yii 1.x** и скопировать папку **yii** в корневой каталог сайта.

Создать папку **runtime** в **protected** и назначить права **755**

Создать папку **assets** в корневой папке сайта и назначить права **755**