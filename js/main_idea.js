/**
 * Created by zwer on 13.08.14.
 */
$(document).ready(function(){
    $(window).resize(function() {

        ellipses1 = $("#bc1 :nth-child(2)")
        if ($("#bc1 a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}

        ellipses2 = $("#bc2 :nth-child(2)")
        if ($("#bc2 a:hidden").length >0) {ellipses2.show()} else {ellipses2.hide()}

    })

});

$(function () {
    $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function(event) {
        var $panel = $(this).closest('.panel-google-plus');
        $comment = $panel.find('.panel-google-plus-comment');

        $comment.find('.btn:first-child').addClass('disabled');
        $comment.find('textarea').val('');

        $panel.toggleClass('panel-google-plus-show-comment');

        if ($panel.hasClass('panel-google-plus-show-comment')) {
            $comment.find('textarea').focus();
        }
    });
    $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
        var $comment = $(this).closest('.panel-google-plus-comment');

        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
    });
});

$(document).ready(function(){
    $('.like').click(function(){
        var buttonClick = $(this);
        $.ajax({
            type: 'POST',
            url: '/site/like',
            dataType: 'json',
            data:{
                'id_idea' : $(this).data('ideaId')
            },
            success: function(response) {
                if (response.success) {
                    buttonClick.children(".label-primary").html(response.message);
                    if (response.imlike) {
                        buttonClick.find('.fa-thumbs-o-up').addClass('show-fade');
                        buttonClick.find('.fa-thumbs-o-down').removeClass('show-fade');
                    } else {
                        buttonClick.find('.fa-thumbs-o-up').removeClass('show-fade');
                        buttonClick.find('.fa-thumbs-o-down').addClass('show-fade');
                    }
                }
                else console.log("Ups!");
            }
        });
    });
});

jQuery(document).ready(function ($){
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: 'Scroll UP', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
});